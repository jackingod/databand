package com.ruoyi.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 站点对象 databand_site
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandSite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 站点编码 */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String code;

    /** 站点名称 */
    @Excel(name = "站点名称")
    private String title;

    /** 所属部门 */
    @Excel(name = "所属部门")
    private Long deptid;

    /** 所属角色 */
    @Excel(name = "所属角色")
    private Long roleid;

    /** 模板id */
    @Excel(name = "模板id")
    private Long templateid;

    /** 站点地址 */
    @Excel(name = "站点地址")
    private String url;

    /** 首页 */
    @Excel(name = "首页")
    private String homepage;

    /** 描述 */
    @Excel(name = "描述")
    private String descri;

    /** 动作 */
    @Excel(name = "动作")
    private String action;

    /** 图标地址 */
    @Excel(name = "图标地址")
    private String icon;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDeptid(Long deptid) 
    {
        this.deptid = deptid;
    }

    public Long getDeptid() 
    {
        return deptid;
    }
    public void setRoleid(Long roleid) 
    {
        this.roleid = roleid;
    }

    public Long getRoleid() 
    {
        return roleid;
    }
    public void setTemplateid(Long templateid) 
    {
        this.templateid = templateid;
    }

    public Long getTemplateid() 
    {
        return templateid;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setHomepage(String homepage) 
    {
        this.homepage = homepage;
    }

    public String getHomepage() 
    {
        return homepage;
    }
    public void setDescri(String descri) 
    {
        this.descri = descri;
    }

    public String getDescri() 
    {
        return descri;
    }
    public void setAction(String action) 
    {
        this.action = action;
    }

    public String getAction() 
    {
        return action;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("title", getTitle())
            .append("deptid", getDeptid())
            .append("roleid", getRoleid())
            .append("templateid", getTemplateid())
            .append("url", getUrl())
            .append("homepage", getHomepage())
            .append("descri", getDescri())
            .append("action", getAction())
            .append("icon", getIcon())
            .toString();
    }
}
