package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandSitehistroy;
import com.ruoyi.web.service.IDatabandSitehistroyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站点历史Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/sitehistroy")
public class DatabandSitehistroyController extends BaseController
{
    private String prefix = "web/sitehistroy";

    @Autowired
    private IDatabandSitehistroyService databandSitehistroyService;

    @RequiresPermissions("web:sitehistroy:view")
    @GetMapping()
    public String sitehistroy()
    {
        return prefix + "/sitehistroy";
    }

    /**
     * 查询站点历史列表
     */
    @RequiresPermissions("web:sitehistroy:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandSitehistroy databandSitehistroy)
    {
        startPage();
        List<DatabandSitehistroy> list = databandSitehistroyService.selectDatabandSitehistroyList(databandSitehistroy);
        return getDataTable(list);
    }

    /**
     * 导出站点历史列表
     */
    @RequiresPermissions("web:sitehistroy:export")
    @Log(title = "站点历史", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandSitehistroy databandSitehistroy)
    {
        List<DatabandSitehistroy> list = databandSitehistroyService.selectDatabandSitehistroyList(databandSitehistroy);
        ExcelUtil<DatabandSitehistroy> util = new ExcelUtil<DatabandSitehistroy>(DatabandSitehistroy.class);
        return util.exportExcel(list, "sitehistroy");
    }

    /**
     * 新增站点历史
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
    	Long userId = ShiroUtils.getUserId();
    	mmap.put("userId", userId);
        return prefix + "/add";
    }

    /**
     * 新增保存站点历史
     */
    @RequiresPermissions("web:sitehistroy:add")
    @Log(title = "站点历史", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandSitehistroy databandSitehistroy)
    {
        return toAjax(databandSitehistroyService.insertDatabandSitehistroy(databandSitehistroy));
    }

    /**
     * 修改站点历史
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandSitehistroy databandSitehistroy = databandSitehistroyService.selectDatabandSitehistroyById(id);
        mmap.put("databandSitehistroy", databandSitehistroy);
        return prefix + "/edit";
    }

    /**
     * 修改保存站点历史
     */
    @RequiresPermissions("web:sitehistroy:edit")
    @Log(title = "站点历史", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandSitehistroy databandSitehistroy)
    {
        return toAjax(databandSitehistroyService.updateDatabandSitehistroy(databandSitehistroy));
    }

    /**
     * 删除站点历史
     */
    @RequiresPermissions("web:sitehistroy:remove")
    @Log(title = "站点历史", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandSitehistroyService.deleteDatabandSitehistroyByIds(ids));
    }
}
