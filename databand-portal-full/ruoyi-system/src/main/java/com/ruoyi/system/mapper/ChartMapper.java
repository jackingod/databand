package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.DyDataset;


public interface ChartMapper {
	
	 public List<DyDataset> selectDatasetList(@Param("datetype")String datetype,@Param("productline")String productline,@Param("channel")String channel);
}
