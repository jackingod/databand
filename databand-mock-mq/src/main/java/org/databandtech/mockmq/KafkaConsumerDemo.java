package org.databandtech.mockmq;

import java.util.Properties;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.databandtech.mockmq.kafka.SyncCommitConsumer;

/**
 * 如果报错请修改server.properties，设置IP：
 * listeners=PLAINTEXT://192.168.13.52:9092
 * @author Administrator
 *
 */
public class KafkaConsumerDemo {
	
	static String TOPIC="Hello-Kafka";
    static Properties props = new Properties();
	static String HOST="192.168.13.52:9092";//"192.168.10.60:9092"

    static {
        props.put("bootstrap.servers", HOST);
        props.put("group.id", "Test001");
        props.put("enable.auto.commit", false);//注意这里设置为手动提交方式
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    }

	public static void main(String[] args) {
		
	
		SyncCommitConsumer consumer = new SyncCommitConsumer(new KafkaConsumer<String,String>(props),TOPIC);
		consumer.printReceiveMsg();		   
		   
	}

}
