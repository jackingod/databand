package org.databandtech.logmock.entity;

public class ProductOrderSale {

	String productId;
	String categoryId;
	String modelId;
	String color;
	String userId;
	String saleDatetime;
	int buyCount;
	int buyTotle;
	int buyDiscount;
	String cityCode;
	String address;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSaleDatetime() {
		return saleDatetime;
	}

	public void setSaleDatetime(String saleDatetime) {
		this.saleDatetime = saleDatetime;
	}

	public int getBuyCount() {
		return buyCount;
	}

	public void setBuyCount(int buyCount) {
		this.buyCount = buyCount;
	}

	public int getBuyTotle() {
		return buyTotle;
	}

	public void setBuyTotle(int buyTotle) {
		this.buyTotle = buyTotle;
	}

	public int getBuyDiscount() {
		return buyDiscount;
	}

	public void setBuyDiscount(int buyDiscount) {
		this.buyDiscount = buyDiscount;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
