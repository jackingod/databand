package org.databandtech.api.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 站点历史对象 databand_sitehistroy
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandSitehistroy 
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 站点ID */
    private Long siteid;

    /** 对象值 */
    private String obj;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updatime;
    private String updateby;

    public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSiteid(Long siteid) 
    {
        this.siteid = siteid;
    }

    public Long getSiteid() 
    {
        return siteid;
    }
    public void setObj(String obj) 
    {
        this.obj = obj;
    }

    public String getObj() 
    {
        return obj;
    }
    public void setUpdatime(Date updatime) 
    {
        this.updatime = updatime;
    }

    public Date getUpdatime() 
    {
        return updatime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("siteid", getSiteid())
            .append("obj", getObj())
            .append("updatime", getUpdatime())
            .append("updateby", getUpdateby())
            .toString();
    }
}
