package org.databandtech.api.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 报版本对象 databand_reporthistroy
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandReporthistroy
{
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long reportid;
    private String obj;
    private Date updatime;
    private String updateby;

    public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

	public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReportid(Long reportid) 
    {
        this.reportid = reportid;
    }

    public Long getReportid() 
    {
        return reportid;
    }
    public void setObj(String obj) 
    {
        this.obj = obj;
    }

    public String getObj() 
    {
        return obj;
    }
    public void setUpdatime(Date updatime) 
    {
        this.updatime = updatime;
    }

    public Date getUpdatime() 
    {
        return updatime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("reportid", getReportid())
            .append("obj", getObj())
            .append("updatime", getUpdatime())
            .append("updateby", getUpdateby())
            .toString();
    }
}
