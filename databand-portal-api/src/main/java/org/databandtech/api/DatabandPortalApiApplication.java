package org.databandtech.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabandPortalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabandPortalApiApplication.class, args);
	}

}
