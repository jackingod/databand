package org.databandtech.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * 注入见TaskConfig的方法scheduledTaskJobMap() 的例子，目前仅提供java注入，未来有数据库加载注入和配置文件注入
命令行任务，CommandExecuteJob的实例，
原始记录备份（从本地），从数据源中备份原始数据到HDFS，HdfsBackupJob;
原始记录备份（到本地），从HDFS数据源中备份原始数据到本地文件，HdfsToLocalFileJob;
Hive ETL任务，从备份HDFS或其他数据源中ETL获取转换数据到到HIVE数据仓库，EtlToHiveJob;
Hive SQL任务，HiveSqlQueryJob，hive执行DQL查询任务，需要返回数据集，并对结果存储，存储的数据用于报表图表等展现;
Hive SQL任务，HiveSqlExecuteJob，hive执行脚本任务，不是查询任务，不需要返回数据集，用于DDL、DML操作，比如load data等,必须实现SavableTaskJob接口;
统计分析计算，Hadoop中运行MR，执行处理，HadoopMRJob;
统计分析计算，Spark中运行MR，执行处理，SparkJob;
统计分析计算，Flink中运行转换萃取清洗以及MR计算，FlinkJob;
数据汇聚入分析数据仓 OLAP，第一次初始化全量任务，从HDFS或Hive将统计分析结果导入OLAP库，如ClickHouse，HdfsToClickHouseFullJob;
数据汇聚入分析数据仓 OLAP，增量任务，从HDFS或Hive将统计分析结果导入OLAP库，如ClickHouse，HdfsToClickHouseDailyJob;
数据汇聚入分析数据仓 OLAP，第一次初始化全量任务，从HDFS或Hive将统计分析结果导入OLAP库，如Mysql、Oracle，HdfsToJdbcFullJob;
数据汇聚入分析数据仓 OLAP，增量任务，从HDFS或Hive将统计分析结果导入OLAP库，如Mysql、Oracle，HdfsToJdbcDailyJob;

运行方式：
1、先导入数据：databand_scheduletask.sql
2、查看任务：
http://localhost:8081/getAllSchedule
3、启动单一任务，目前还没有统一的管理界面，未来会开发完善：
http://localhost:8081/start?jobcode=WindowsDir1
http://localhost:8081/start?jobcode=WindowsIP1
http://localhost:8081/start?jobcode=hdfs_product2020
http://localhost:8081/start?jobcode=hdfs_toLocal2020
http://localhost:8081/start?jobcode=hdfs_toLocal2020_1

 * @author Administrator
 *
 */

@EnableAutoConfiguration
@SpringBootApplication
public class DatabandJobSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabandJobSpringbootApplication.class, args);
	}

}